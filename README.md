# Diplom Portal API :milky_way:

#### Prerequisite:
Docker & docker-compose

#### Getting started:
Pull Docker image :arrow_down: : 

### `docker pull registry.gitlab.com/kameldrengene/diplomportalapi`

Diplom Portal API has to connect to a MySql database :floppy_disk: :

### `docker run -d -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=Diplomportal --name diplomdb mysql`

and run Diplom Portal Api:

### `docker run -d -p 8080:8080 --name diplomapi registry.gitlab.com/kameldrengene/diplomportalapi` 

or with docker-compose:

### `docker-compose up -d`

This docker-compose command will pull and deploy necessarry database and build an image with project source.
Further builds with project source can be done with:
### `docker-compose up -d --build`


#### CI/CD Pipeline:
```mermaid
flowchart TD

build --> test
test --> docker-build
docker-build --> Gitlabregistry{{registry.gitlab.com/kameldrengene/diplomportalapi}}


subgraph "docker-build"
DockerbuildNode1[docker login gitlab] --> DockerbuildNode2[docker build]
DockerbuildNode2 --> DockerbuildNode3[docker push]
end


subgraph "test"
TestNode1[mvn test]
end

subgraph "build"
Node1[mvn package -B]
end

```

#### Sequence Diagram getStudents()
```mermaid
sequenceDiagram
    Client->>JwtRequestFilter: Post/authenticate with username and password
    JwtRequestFilter->>JwtRequestFilter: Check if request has token
    alt has token
        JwtRequestFilter->>JwtUtil: Extract User name
        JwtUtil->>JwtRequestFilter: Returns User name
    else has not token
        JwtRequestFilter->>Client: Return status 403
    end
    JwtRequestFilter->>AuthUserDetailService: Load UserDetails using the username
    alt username exist
        AuthUserDetailService->>JwtRequestFilter: Return UserDetails
    else username not exist
        AuthUserDetailService->>Client: Returns 404 Resource not found
    end
    JwtRequestFilter->>JwtUtil: Validate the Jwt Token
    alt valid token
        JwtUtil->>JwtRequestFilter: Return true 
    else not valid token
        JwtUtil->>JwtRequestFilter: Return false
        JwtRequestFilter->>Client: Return 403 not authorized
    end
    JwtRequestFilter->>JwtRequestFilter: Configure Spring security for User
    JwtRequestFilter->>CoursesService: getCourses () method with /courses/ mapping is called
    CoursesService->>Client: Return List<Courses>
```
 
