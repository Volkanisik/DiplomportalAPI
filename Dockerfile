FROM maven:3.6-jdk-13-alpine as build
COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/
RUN mvn package -e

FROM openjdk:13-alpine
WORKDIR /app
COPY --from=build /build/target/DiplomportalAPI-0.0.1-SNAPSHOT.jar /app/
EXPOSE 8080
ENTRYPOINT ["java","-jar","DiplomportalAPI-0.0.1-SNAPSHOT.jar"]