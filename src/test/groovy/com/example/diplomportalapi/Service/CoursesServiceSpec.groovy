package com.example.diplomportalapi.Service
import com.example.diplomportalapi.Entity.Courses
import com.example.diplomportalapi.Entity.Weeks
import com.example.diplomportalapi.Entity.WeeksId
import com.example.diplomportalapi.repository.CourseRepository
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Unroll


class CoursesServiceSpec extends Specification {


    private static final WEEK_1 = new Weeks(description: 'Opgave 1',id:(new WeeksId(year:2021)))
    private static final WEEK_2 = new Weeks(description: 'Opgave 2',id:(new WeeksId(year:2021)))
    private static final WEEK_3 = new Weeks(description: 'Opgave 3',id:(new WeeksId(year:2021)))
    private static final WEEK_4 = new Weeks(description: 'Opgave 4',id:(new WeeksId(year:2021)))
    private static final COURSE_1 = new Courses(name: 'Software',weeks: [WEEK_1,WEEK_2,WEEK_3,WEEK_4])
    private static final COURSE_2 = new Courses(name: 'Devops',weeks: [WEEK_1,WEEK_2,WEEK_3,WEEK_4])
    private static final COURSE_3 = new Courses(name: 'C-sharp',weeks: [WEEK_1,WEEK_2,WEEK_3,WEEK_4])
    private static final COURSE_4 = new Courses(name: 'C++',weeks: [WEEK_1,WEEK_2,WEEK_3,WEEK_4])

    // private CoursesService coursesService
    private CoursesService coursesService = new CoursesService(
            courseRepository: Mock(CourseRepository)
    )

/*
    void setup() {
        coursesService = new CoursesService()
        coursesService.courseRepository = Mock(CourseRepository)
    }


 */

    /*def "Can get the weeks of a course a certain year"(){
        given:
        coursesService.courseRepository.getById(36).getWeeks() >> [WEEK_1, WEEK_2, WEEK_3]
        coursesService.courseRepository.getById(37).getWeeks() >> [WEEK_2, WEEK_3, WEEK_4]
        when:
        def output = coursesService.getWeeks(36, 2021)
        def output2 = coursesService.getWeeks(37, 2021)
        then:
        output == [WEEK_1, WEEK_2, WEEK_3]
        output2 == [WEEK_2, WEEK_3, WEEK_4]
    }*/

    def "Can get courses from Course Repository"() {
        given:
        coursesService.courseRepository.findAll() >> [COURSE_1, COURSE_2, COURSE_3]

        when:
        def output = coursesService.getCourses()

        then:
        output == [COURSE_1, COURSE_2, COURSE_3]
    }

    @Unroll
    def 'Can find a course name by id #name'() {
        given:

        when:
        def output = coursesService.getCourseName(id)

        then: 'The repository returns a course'
        1 * coursesService.courseRepository.getById(_) >> { int cid ->
            switch (cid) {
                case 36: return COURSE_1
                case 37: return COURSE_2
                default: assert false
            }
        }

        and:
        output.string == name

        where:
        id | name
        36 | 'Software'
        37 | 'Devops'
    }
}
