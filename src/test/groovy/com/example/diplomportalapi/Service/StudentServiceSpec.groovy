package com.example.diplomportalapi.Service

import com.example.diplomportalapi.Entity.Students
import com.example.diplomportalapi.repository.StudentsRepository
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification


class StudentServiceSpec extends Specification {

    private static final STUDENT_1 = new Students(firstName: 'Mark')
    private static final STUDENT_2 = new Students(firstName: 'Talha')
    private static final STUDENT_3 = new Students(firstName: 'Volkan')
    private static final STUDENT_4 = new Students(firstName: 'Mikkel')

    private StudentService studentService = new StudentService(
            studentRepository: Mock(StudentsRepository)
    )

    def 'Can get Students from Student Service'() {
        given:
        studentService.studentRepository.findAll() >> [STUDENT_1, STUDENT_2, STUDENT_3, STUDENT_4]

        when:
        def output = studentService.getStudents()

        then: 'The repository returns all Students'
        output == [STUDENT_1, STUDENT_2, STUDENT_3, STUDENT_4]
    }


}
