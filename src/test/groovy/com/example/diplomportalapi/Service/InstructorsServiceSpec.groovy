package com.example.diplomportalapi.Service

import com.example.diplomportalapi.Entity.Instructors
import com.example.diplomportalapi.repository.InstructorsRepository
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification


class InstructorsServiceSpec extends Specification {

    private static final INSTRUCTOR_1 = new Instructors(firstName: 'Mark')
    private static final INSTRUCTOR_2 = new Instructors(firstName: 'Talha')
    private static final INSTRUCTOR_3 = new Instructors(firstName: 'Volkan')
    private static final INSTRUCTOR_4 = new Instructors(firstName: 'Mikkel')

    private InstructorsService instructorsService = new InstructorsService(
            instructorsRepository: Mock(InstructorsRepository)
    )

    def 'Can get instructors from Instructor Service'() {
        given:
        instructorsService.instructorsRepository.findAll() >> [INSTRUCTOR_1, INSTRUCTOR_2, INSTRUCTOR_3, INSTRUCTOR_4]

        when:
        def output = instructorsService.getInstructors()

        then: 'The repository returns all instructors'
        output == [INSTRUCTOR_1, INSTRUCTOR_2, INSTRUCTOR_3, INSTRUCTOR_4]
    }


}
