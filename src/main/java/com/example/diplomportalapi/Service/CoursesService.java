package com.example.diplomportalapi.Service;

import com.example.diplomportalapi.DTO.StringDTO;
import com.example.diplomportalapi.Entity.Courses;
import com.example.diplomportalapi.Entity.Instructors;
import com.example.diplomportalapi.Entity.Weeks;
import com.example.diplomportalapi.Metrics.Metrics;
import com.example.diplomportalapi.repository.CourseRepository;
import com.example.diplomportalapi.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/courses")
public class CoursesService {

    @Autowired
    CourseRepository courseRepository;

    @GetMapping("/")
    public List<Courses> getCourses (){
        Metrics.courseRequests.inc();
        return courseRepository.findAll();
    }

    // Get the name of a course
    @GetMapping(path="/name/{id}")
    public StringDTO getCourseName(@PathVariable("id") int id) {
        Metrics.courseRequests.inc();
        return new StringDTO(courseRepository.getById(id).getName());
    }

    // Get the instructor for by course id
    @GetMapping(path="/{id}/instructor")
    public Instructors getCourseInstructor(@PathVariable("id") int id) {
        Metrics.courseRequests.inc();
        return courseRepository.getById(id).getInstructors();
    }


    // Get the weeks of a course a certain year
    @GetMapping(path="/{id}/year/{year}")
    public List<Weeks> getWeeks(@PathVariable("id") int id, @PathVariable("year") int year) {
        List<Weeks> allWeeks = courseRepository.getById(id).getWeeks();
        Metrics.courseRequests.inc();
        List<Weeks> yearWeeks = new ArrayList<>();
        // Loop through all weeks
        for (int i = 0; i < allWeeks.size(); i++) {
            if(allWeeks.get(i).getYear() == year)
                yearWeeks.add(allWeeks.get(i));
        }

        return yearWeeks;
    }


    @PostMapping("/")
    private Courses createCourse(@RequestBody Courses courses){
        Metrics.courseRequests.inc();
        return courseRepository.save(courses);
    }

}
