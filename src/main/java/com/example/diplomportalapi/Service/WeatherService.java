package com.example.diplomportalapi.Service;

import com.example.diplomportalapi.Exceptions.WeatherException;
import com.example.diplomportalapi.Metrics.Metrics;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


@RestController
@RequestMapping("/weather")
public class WeatherService {

    @GetMapping("/")
    public Map<String, String> getWeather () {
        Metrics.weatherRequests.inc();

        try {
            URL url = new URL("https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=55.73107&lon=12.39702");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", "Diplomportal");
            conn.connect();

            //Getting the response code
            int responsecode = conn.getResponseCode();

            if (responsecode != 200)
                throw new WeatherException("Did not get expected response from weather server. HttpResponseCode: " + responsecode);

            String inline = "";
            Scanner scanner = new Scanner(url.openStream());

            //Write all the JSON data into a string using a scanner
            while (scanner.hasNext()) {
                inline += scanner.nextLine();
            }

            //Close the scanner
            scanner.close();

            //Using the JSON simple library parse the string into a json object
            JSONObject json = new JSONObject(inline);
            JSONObject properties = json.getJSONObject("properties");
            JSONArray timeSeries = properties.getJSONArray("timeseries");
            JSONObject timeStep = timeSeries.getJSONObject(0);
            String time = timeStep.getString("time");
            JSONObject data = timeStep.getJSONObject("data");
            JSONObject instant = data.getJSONObject("instant");
            JSONObject details = instant.getJSONObject("details");
            String temperature = details.getString("air_temperature");
            JSONObject nextHour =  data.getJSONObject("next_1_hours");
            JSONObject summary = nextHour.getJSONObject("summary");
            String weatherCode = summary.getString("symbol_code");

            // Return the data
            HashMap<String, String> map = new HashMap<>();
            map.put("time", time);
            map.put("air_temperature", temperature);
            map.put("weather_code", weatherCode);
            return map;

        } catch (Exception e) {
            e.printStackTrace();
            throw new WeatherException("Error while connecting to and parsing input from weather server" );
        }
    }
}
