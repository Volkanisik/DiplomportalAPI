package com.example.diplomportalapi.Service;

import com.example.diplomportalapi.Entity.Weeks;
import com.example.diplomportalapi.Metrics.Metrics;
import com.example.diplomportalapi.repository.WeeksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/weeks")
public class WeeksService {

    @Autowired
    private WeeksRepository weeksRepository;


    @GetMapping("/")
    public List<Weeks> getWeeks (){
        Metrics.weekRequests.inc();
        return weeksRepository.findAll();
    }

    @PostMapping("/")
    public @ResponseBody
    Weeks CreateWeek(@RequestBody Weeks weeks){
        Metrics.weekRequests.inc();
        return weeksRepository.save(weeks);
    }
}