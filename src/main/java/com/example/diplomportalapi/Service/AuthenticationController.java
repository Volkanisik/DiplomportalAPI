package com.example.diplomportalapi.Service;
import com.example.diplomportalapi.Authentication.Models.AuthenticationRequest;
import com.example.diplomportalapi.Authentication.Models.AuthenticationResponse;
import com.example.diplomportalapi.Authentication.Services.AuthUserDetailsService;
import com.example.diplomportalapi.Authentication.Util.JwtUtil;
import com.example.diplomportalapi.Metrics.Metrics;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.Collections;

@RestController
public class AuthenticationController {

    @Value("${API_URL}")
    private String apiUrl;

    @Value("${CLIENT_URL}")
    private String clientUrl;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private AuthUserDetailsService userDetailsService;

    @RequestMapping({ "/hello" })
    public String firstPage() {
        return "Hello World";
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) {

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String jwt = jwtTokenUtil.generateToken(userDetails);
        Metrics.authenticationRequests.inc();
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    //https://stackoverflow.com/questions/34252848/response-seeothers-in-spring-rest
    @GetMapping("/login")
    @CrossOrigin(origins = "*")
    ResponseEntity<Void> login() {
        Metrics.attemptCounter.inc();
        Metrics.authenticationRequests.inc();
        return ResponseEntity.status(HttpStatus.FOUND)
                .location(URI.create("https://auth.dtu.dk/dtu/?service="+apiUrl+"/campusnet"))
                .build();
    }

    @GetMapping("/campusnet")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Void> redirect(@RequestParam String ticket){

        String body = Unirest.get( "https://auth.dtu.dk/dtu/validate?service="+apiUrl+"/campusnet&ticket="
                        + ticket)
                .asString()
                .getBody();
        String[] bodyString = body.split("\n");
        boolean authorized = bodyString[0].contains("yes");
        Metrics.authenticationRequests.inc();
        if (authorized) {
            String userId = bodyString[1];
            final UserDetails userDetails = new org.springframework.security.core.userdetails.User(userId,"dtu",Collections.emptyList());
            String token = jwtTokenUtil.generateToken(userDetails);

            return ResponseEntity.status(HttpStatus.FOUND)
                    .location(URI.create(clientUrl + "/?token=" + token))
                    .build();
        }else{
            Metrics.failCounter.inc();
        }
        return login();
    }
}

