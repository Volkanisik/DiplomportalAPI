package com.example.diplomportalapi.Service;

import com.example.diplomportalapi.Entity.Instructors;
import com.example.diplomportalapi.Entity.Students;
import com.example.diplomportalapi.Metrics.Metrics;
import com.example.diplomportalapi.repository.InstructorsRepository;
import com.example.diplomportalapi.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/instructors")
public class InstructorsService {

    @Autowired
    InstructorsRepository instructorsRepository;

    @GetMapping("/")
    public List<Instructors> getInstructors (){
        Metrics.instructorRequests.inc();
        return instructorsRepository.findAll();
    }

    // Get the name, link and email for an instructor
    @GetMapping(path="/name/{id}")
    public String getCourseInstructor(@PathVariable("id") int id) {
        Metrics.instructorRequests.inc();
        String out = "";
        Instructors instructors = instructorsRepository.getById(id);
        out += "{\n";
        out += "\t" + "\"name\": " + instructors.getFirstName() + " " + instructors.getLastName() + ",\n";
        out += "\t" + "\"link\": " + instructors.getLink() + ",\n";
        out += "\t" + "\"email\": " + instructors.getMail() + "\n";
        out += "}";

        return out;

    }

    @PostMapping("/")
    public @ResponseBody
    Instructors CreateInstructor(@RequestBody Instructors instructors){
        Metrics.instructorRequests.inc();
        return instructorsRepository.save(instructors);
    }

}

