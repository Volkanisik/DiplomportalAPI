package com.example.diplomportalapi.Service;

import com.example.diplomportalapi.Authentication.Util.JwtUtil;
import com.example.diplomportalapi.DTO.CoursesByStudentId;
import com.example.diplomportalapi.DTO.StringDTO;
import com.example.diplomportalapi.DTO.TakesJoinCourse;
import com.example.diplomportalapi.Entity.Courses;
import com.example.diplomportalapi.Entity.Students;
import com.example.diplomportalapi.Entity.Takes;
import com.example.diplomportalapi.Metrics.Metrics;
import com.example.diplomportalapi.pdf.PdfExporter;
import com.example.diplomportalapi.repository.StudentsRepository;
import com.example.diplomportalapi.repository.TakesJoinRepository;
import com.example.diplomportalapi.repository.TakesRepository;
import org.dom4j.DocumentException;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentService {

    @Autowired
    StudentsRepository studentRepository;

    @Autowired
    private TakesJoinRepository takesJoinRepository;

    @Autowired
    private JwtUtil jwtUtil;
//TODO: Denne endpoint bruges ikke i vores frontend.
    @GetMapping("/")
    public List<Students> getStudents (){
        Metrics.studentRequests.inc();
        return studentRepository.findAll();
    }

    @PostMapping("/")
    public @ResponseBody
    Students CreateStudent(@RequestBody Students students){
        Metrics.studentRequests.inc();
        return studentRepository.save(students);
    }

    @GetMapping("/student")
    public Students getStudentById(@RequestHeader("authorization") String token) throws Exception {
        Metrics.studentRequests.inc();
        int id = jwtUtil.extractUserId(token);
        List<Students> students = studentRepository.findAll();
        for (Students student:students) {
            if(student.getId() == id)
                return student;
        }
        throw new Exception("resource not found");

    }

    // Get the courses from a certain year
    @GetMapping(path="/course/{year}")
    public List<Courses> getCourses(@PathVariable("year") int year,
                                    @RequestHeader("authorization") String token) {
        Metrics.studentRequests.inc();
        int id = jwtUtil.extractUserId(token);
        List<Takes> takes = studentRepository.getById(id).getTakes();

        List<Courses> courses = new ArrayList<>();
        // Loop through all courses taken
        for (int i = 0; i < takes.size(); i++) {
            if(takes.get(i).getYear() == year)
                courses.add(takes.get(i).getCourses());
        }

        return courses;
    }
    // Get all courses for a student
    @GetMapping(path="/courses")
    public List<CoursesByStudentId> getAllCourses(@RequestHeader("authorization") String token) {

        Metrics.studentRequests.inc();
        int id = jwtUtil.extractUserId(token);
        List<CoursesByStudentId> courses = takesJoinRepository.getCourseswithYearByStudents_id(id);
        return courses;
    }

    @GetMapping(path="/grades")
    public List<TakesJoinCourse> getJoinInformation(@RequestHeader("authorization") String token) {

        Metrics.studentRequests.inc();
        int id = jwtUtil.extractUserId(token);
        List<TakesJoinCourse> grades = takesJoinRepository.getJoinInformation();

        List<TakesJoinCourse> studentgrades = new ArrayList<>();
        // Loop through all grades and get by student id
        for (TakesJoinCourse grade : grades) {
            if(grade.getStudents_id() == id && grade.getGrade() != -1){
                studentgrades.add(grade);
            }
        }

        return studentgrades;
    }

    @PostMapping(value = "/update/takes")
    public StringDTO updateTakes(@RequestBody List<Takes> takes,
                          @RequestHeader("authorization") String token) {

        Metrics.studentRequests.inc();
        int studentId = jwtUtil.extractUserId(token);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        // todo Check no duplicates
        // todo Check the sum of ECTS is 180
        // todo check years are within limit


        // Make room for new takes in the database
        studentRepository.deleteFutureTakes(studentId, currentYear);

        // Add the new takes to the database
        for (int i = 0; i < takes.size(); i++) {
            Takes take = takes.get(i);
            if (take.getYear() > currentYear){
                studentRepository.insertTake(
                        take.getCourses_id(),
                        studentId,
                        take.getYear(),
                        -1
                );
            }
        }
        return new StringDTO("DONE");
    }

    @GetMapping(path = "/studyPlaner/courses")
    public List<TakesJoinCourse> getStudentsStudyPlanCourses(@RequestHeader("authorization") String token) throws IOException {
        Metrics.studentRequests.inc();
        int id = jwtUtil.extractUserId(token);
        List<TakesJoinCourse> courses = takesJoinRepository.getJoinInformation();
        List<TakesJoinCourse> studentStudyPlanCourses = new ArrayList<>();

        for (TakesJoinCourse course : courses) {
            if(course.getStudents_id() == id){
                studentStudyPlanCourses.add(course);
            }
        }
        return studentStudyPlanCourses;
    }


    @GetMapping(path = "/export")
    public void exportToPDF(HttpServletResponse response, @RequestHeader("authorization") String token) throws DocumentException, IOException{
        Metrics.studentRequests.inc();
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        int id = jwtUtil.extractUserId(token);
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=s"+ id+ "_grades_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);


        List<TakesJoinCourse> grades = takesJoinRepository.getJoinInformation();

        List<TakesJoinCourse> studentgrades = new ArrayList<>();
        // Loop through all grades and get by student id
        for (TakesJoinCourse grade : grades) {
            if(grade.getStudents_id() == id){
                studentgrades.add(grade);
            }
        }

        PdfExporter exporter = new PdfExporter(studentgrades);
        exporter.export(response,id);

    }
}
