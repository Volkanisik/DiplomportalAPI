package com.example.diplomportalapi.Service;

import com.example.diplomportalapi.Metrics.Metrics;
import io.prometheus.client.CollectorRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringWriter;

@RestController
public class PrometheusService {

    @RequestMapping(
            value = "/metrics",
            method = RequestMethod.GET,
            produces="text/plain"
    )
    public String getPrometheusData() {
        Metrics.attemptCounter.inc(); // Hack - Data reporting only starts after one counter has been altered
        StringWriter writer = new StringWriter();
        try {
            io.prometheus.client.exporter.common.TextFormat.write004(
                    writer, CollectorRegistry.defaultRegistry.metricFamilySamples());
        } catch (Exception e) {
            return e.getMessage();
        }
        return writer.toString();
    }

}

