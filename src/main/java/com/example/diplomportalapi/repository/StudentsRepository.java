package com.example.diplomportalapi.repository;
import com.example.diplomportalapi.Entity.Students;

import com.example.diplomportalapi.Entity.Takes;
import lombok.Data;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
public interface StudentsRepository extends JpaRepository<Students,Integer> {

    // Deletes all the takes a student has not begun yet
    @Transactional
    @Modifying
    @Query("DELETE FROM Takes t WHERE t.id.students_id = :id AND t.id.year > :year")
    public void deleteFutureTakes(@Param("id") int id,
                                  @Param("year") int year);

    // Returns a list of all the course ids a student cannot change anymore
    @Query("SELECT t.courses.id FROM Takes t WHERE t.id.students_id = :id AND t.id.year <= :year")
    public List<Integer> getLockedCourses(@Param("id") int id,
                                          @Param("year") int year);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO takes VALUES(:course_id, :student_id, :year, :grade);", nativeQuery = true)
    public void insertTake(
            @Param("course_id") int course_id,
            @Param("student_id") int student_id,
            @Param("year") int year,
            @Param("grade") int grade
    );
}




