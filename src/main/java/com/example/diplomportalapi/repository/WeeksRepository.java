package com.example.diplomportalapi.repository;

import com.example.diplomportalapi.Entity.Weeks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeeksRepository extends JpaRepository<Weeks,Integer> {
}
