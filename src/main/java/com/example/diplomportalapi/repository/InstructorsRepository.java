package com.example.diplomportalapi.repository;
import com.example.diplomportalapi.Entity.Instructors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface InstructorsRepository extends JpaRepository<Instructors,Integer> {
}
