package com.example.diplomportalapi.repository;

import com.example.diplomportalapi.Entity.Takes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TakesRepository extends JpaRepository<Takes,Integer> {
}
