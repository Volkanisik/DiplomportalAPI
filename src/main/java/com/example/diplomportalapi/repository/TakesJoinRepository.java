package com.example.diplomportalapi.repository;

import com.example.diplomportalapi.DTO.CoursesByStudentId;
import com.example.diplomportalapi.DTO.TakesJoinCourse;
import com.example.diplomportalapi.Entity.Takes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface TakesJoinRepository extends JpaRepository<Takes,Integer> {
    @Query("SELECT new com.example.diplomportalapi.DTO.TakesJoinCourse(t.grade,t.id.students_id ,t.id.courses_id ,t.courses.name,t.id.year,t.courses.day,t.courses.time,t.courses.ects) FROM Takes t JOIN t.courses c ORDER BY t.id.year asc ")
    public List<TakesJoinCourse> getJoinInformation();

    @Query("SELECT new com.example.diplomportalapi.DTO.CoursesByStudentId(t.id.courses_id ,t.courses.name,t.id.year,t.courses.day,t.courses.time,t.courses.image) FROM Takes t JOIN t.courses c where t.id.students_id = :id ORDER BY t.id.year asc")
    public List<CoursesByStudentId> getCourseswithYearByStudents_id(@Param("id") int id);
}
