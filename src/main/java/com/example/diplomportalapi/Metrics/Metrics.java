package com.example.diplomportalapi.Metrics;


import io.prometheus.client.Counter;
import io.prometheus.client.hotspot.*;

public class Metrics {
    public final static Counter requestCounter = Counter.build().name("requestAttempts").help("Total request").register();
    public final static Counter attemptCounter = Counter.build().name("loginAttempts").help("Total Login Attempts").register();
    public final static Counter failCounter = Counter.build().name("loginFails").help("Total Failed Attempts").register();
    public final static Counter tokenvalidated = Counter.build().name("tokenValidated").help("Total Validated Tokens").register();
    public final static Counter tokenfailed = Counter.build().name("tokenFails").help("Total Failed Tokens").register();
    public final static Counter authenticationRequests = Counter.build().name("authenticationRequests").help("Total Authentication Requests").register();
    public final static Counter courseRequests = Counter.build().name("courseRequests").help("Total Course Requests").register();
    public final static Counter instructorRequests = Counter.build().name("instructorRequests").help("Total Instructor Requests").register();
    public final static Counter studentRequests = Counter.build().name("studentRequests").help("Total Student Requests").register();
    public final static Counter weatherRequests = Counter.build().name("weatherRequests").help("Total Weather Requests").register();
    public final static Counter weekRequests = Counter.build().name("weekRequests").help("Total Week Requests").register();
    public final static StandardExports standardExports= new StandardExports().register();
    public final static MemoryPoolsExports memoryPoolsExports =  new MemoryPoolsExports().register();
    public final static MemoryAllocationExports memoryAlocationExports = new MemoryAllocationExports().register();
    public final static BufferPoolsExports bufferPoolsExports = new BufferPoolsExports().register();
    public final static GarbageCollectorExports garbageCollectorExports = new GarbageCollectorExports().register();
    public final static ThreadExports threadExports = new ThreadExports().register();
    public final static ClassLoadingExports classLoadingExports= new ClassLoadingExports().register();
    public final static VersionInfoExports versionInfoExports = new VersionInfoExports().register();
}

