package com.example.diplomportalapi.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StringDTO {
    String string;
}
