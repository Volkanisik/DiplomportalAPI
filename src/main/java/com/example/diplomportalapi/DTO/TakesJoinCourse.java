package com.example.diplomportalapi.DTO;

import com.example.diplomportalapi.Entity.Courses;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TakesJoinCourse {
    private int grade;
    private int students_id;
    private int courses_id;
    private String name;
    private int year;
    private Courses.Day day;
    private Courses.Time time;
    private double ects;

}
