package com.example.diplomportalapi.DTO;

import com.example.diplomportalapi.Entity.Courses;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CoursesByStudentId {
    private int courses_id;
    private String name;
    private int year;
    private Courses.Day day;
    private Courses.Time time;
    private String image;
}
