package com.example.diplomportalapi.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class WeatherException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public WeatherException(String message) { super(message); }
}





