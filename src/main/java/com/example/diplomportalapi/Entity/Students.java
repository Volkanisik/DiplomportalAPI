package com.example.diplomportalapi.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Students {

    // ATTRIBUTES
    @Id
    private int id;

    @Column(nullable = false, length = 45)
    private String firstName;

    @Column(nullable = false, length = 45)
    private String lastName;

    @Column(nullable = false, length = 4)
    private int startYear;


    // CONSTRUCTOR
    public Students() {

    }
    public Students(int id) {
        this.id = id;
    }


    // RELATIONSHIPS
    @OneToMany(mappedBy = "students")
    @JsonIgnore
    private List<Takes> takes;


    // GETTERS AND SETTERS
    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public int getStartYear() { return startYear; }
    public void setStartYear(int startYear) { this.startYear = startYear; }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public List<Takes> getTakes() {
        return takes;
    }
    public void setTakes(List<Takes> takes) {
        this.takes = takes;
    }
}
