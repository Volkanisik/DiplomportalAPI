package com.example.diplomportalapi.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Takes {

    // ATTRIBUTES
    @Column(length = 2)
    private int grade;

    @EmbeddedId
    TakesId id = new TakesId();


    // CONSTRUCTORS
    public Takes() {

    }
    public Takes(int grade, int year, int students_id, int courses_id, Students student, Courses course) {
        this.grade = grade;
        id.setYear(year);
        id.setStudents_id(students_id);
        id.setCourses_id(courses_id);
    }


    // RELATIONSHIPS
    @ManyToOne
    @JsonIgnore
    @MapsId("students_id")
    @JoinColumn(name = "students_id", referencedColumnName = "id")
    private Students students;

    @ManyToOne
    @JsonIgnore
    @MapsId("courses_id")
    @JoinColumn(name = "courses_id", referencedColumnName = "id")
    private Courses courses;

    // GETTERS AND SETTERS
    public int getGrade() {
        return grade;
    }
    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Students getStudents() {
        return students;
    }
    public void setStudents(Students students) {
        this.students = students;
    }

    public Courses getCourses() {
        return courses;
    }
    public void setCourses(Courses courses) {
        this.courses = courses;
    }

    public int getYear() {
        return id.getYear();
    }
    public void setYear(int year) { this.id.setYear(year); }

    public int getStudents_id() {
        return id.getStudents_id();
    }
    public void setStudents_id(int id) {this.id.setStudents_id(id); }

    public int getCourses_id() {
        return id.getCourses_id();
    }
    public void setCourses_id(int id) { this.id.setCourses_id(id); }

    @Override
    public String toString() {
        return "grade: " + grade +
                "\nyear: " + getYear() +
                "\nstudent_id: " + getStudents_id() +
                "\ncourse_id: " + getCourses_id();
    }
}

@Embeddable
class TakesId implements Serializable {
    @Column(length = 4) private int year;
    private int students_id;
    private int courses_id;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getStudents_id() {
        return students_id;
    }

    public void setStudents_id(int students_id) {
        this.students_id = students_id;
    }

    public int getCourses_id() {
        return courses_id;
    }

    public void setCourses_id(int courses_id) {
        this.courses_id = courses_id;
    }
}

