package com.example.diplomportalapi.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.sql.rowset.serial.SerialBlob;
import java.util.List;

@Entity
public class Courses {

    // ATTRIBUTES
    @Id
    private int id;

    @Column(nullable = false, length = 45)
    private String name;

    public enum Day {MON, TUE, WED, THU, FRI}
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Day day;

    public enum Time {MORNING, LUNCH}
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Time time;

    private String image;

    @Column(nullable = false, precision = 3, scale = 1)
    private double ects;


    // CONSTRUCTOR
    public Courses() {
    }
    public Courses(int id) {
        this.id = id;
    }


    // RELATIONSHIPS
    @OneToMany(mappedBy = "courses")
    @JsonIgnore
    private List<Takes> takes;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "instructors_id", referencedColumnName = "id", nullable = false)
    private Instructors instructors;

    @OneToMany(mappedBy = "courses")
    @JsonIgnore
    private List<Weeks> weeks;


    // GETTERS AND SETTERS
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Day getDay() {
        return day;
    }
    public void setDay(Day day) {
        this.day = day;
    }

    public Time getTime() {
        return time;
    }
    public void setTime(Time time) {
        this.time = time;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public double getEcts() {
        return ects;
    }
    public void setEcts(double ects) {
        this.ects = ects;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public List<Takes> getTakes() {
        return takes;
    }
    public void setTakes(List<Takes> takes) {
        this.takes = takes;
    }

    public Instructors getInstructors() {
        return instructors;
    }
    public void setInstructors(Instructors instructors) {
        this.instructors = instructors;
    }

    public List<Weeks> getWeeks() {
        return weeks;
    }
    public void setWeeks(List<Weeks> weeks) {
        this.weeks = weeks;
    }
}
