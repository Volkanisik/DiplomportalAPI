package com.example.diplomportalapi.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Weeks {

    // ATTRIBUTES
    @EmbeddedId
    WeeksId id;

    @Column(length = 300)
    private String description;

    @Column(length = 300)
    private String homework;

    @Column(length = 300)
    private String assignments;

    @Column(length = 300)
    private String files;


    // RELATIONSHIPS
    @ManyToOne
    @JsonIgnore
    @MapsId("course_id")
    @PrimaryKeyJoinColumn(name = "courses_id", referencedColumnName = "id")
    private Courses courses;


    // GETTERS AND SETTERS
    public WeeksId getId() {
        return id;
    }
    public void setId(WeeksId id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomework() {
        return homework;
    }
    public void setHomework(String homework) {
        this.homework = homework;
    }

    public String getAssignments() {
        return assignments;
    }
    public void setAssignments(String assignments) {
        this.assignments = assignments;
    }

    public String getFiles() {
        return files;
    }
    public void setFiles(String files) {
        this.files = files;
    }

    public Courses getCourses() {
        return courses;
    }
    public void setCourses(Courses courses) {
        this.courses = courses;
    }

    public int getYear() {
        return id.getYear();
    }


    public int getWeekNumber() {
        return id.getWeekNumber();
    }


    public int getCourse_id() {
        return id.getCourse_id();
    }

}

// Composite primary key
@Embeddable
class WeeksId implements Serializable {
    @Column(length = 4) private int year;
    @Column(length = 2) private int weekNumber;
    private int course_id;

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public int getWeekNumber() {
        return weekNumber;
    }
    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getCourse_id() {
        return course_id;
    }
    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }
}
