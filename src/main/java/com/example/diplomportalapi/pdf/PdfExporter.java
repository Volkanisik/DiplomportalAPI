package com.example.diplomportalapi.pdf;

import com.example.diplomportalapi.DTO.TakesJoinCourse;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.*;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.io.IOException;
import java.util.List;

public class PdfExporter {

    private List<TakesJoinCourse> grades;

    public PdfExporter( List<TakesJoinCourse> grades){
        this.grades = grades;
    }
    //Writes Table Header
    private void writeTableHeader(PdfPTable table){
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.RED);
        cell.setPadding(5);
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);

        cell.setPhrase(new Phrase("Course ID", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Course Name", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Year", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("ECTS", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Grade", font));
        table.addCell(cell);

    }
    //Writes table row
    private void writeTableData(PdfPTable table) {
        for (TakesJoinCourse grade : grades) {
            if(grade.getGrade() != -1) {
                table.addCell(String.valueOf(grade.getCourses_id()));
                table.addCell(grade.getName());
                table.addCell(String.valueOf(grade.getYear()));
                table.addCell(String.valueOf(grade.getEcts()));
                table.addCell(String.valueOf(grade.getGrade()));
            }
        }
    }
    //Sets general settings as font,color, size etc and exports as response
    public void export(HttpServletResponse response,int id) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLACK);

        Paragraph p = new Paragraph("Karakteroversigt s"+id, font);
        p.setAlignment(Paragraph.ALIGN_CENTER);

        document.add(p);

        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100f);
        table.setWidths(new float[] {1.5f, 3.5f, 3.0f, 3.0f, 1.5f});
        table.setSpacingBefore(10);

        writeTableHeader(table);
        writeTableData(table);

        document.add(table);

        document.close();

    }

}
