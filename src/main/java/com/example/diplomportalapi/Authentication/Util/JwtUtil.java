package com.example.diplomportalapi.Authentication.Util;

import com.example.diplomportalapi.Exceptions.InvalidAuthorizationHeader;
import com.example.diplomportalapi.Exceptions.UserIdException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtUtil {
    @Value("${SECRET}")
    private String SECRET_KEY;

    public String extractUsername(String token) throws UserIdException {
        try{
            return extractClaim(token, Claims::getSubject);
        } catch (Exception e){
            e.printStackTrace();
            throw new UserIdException();
        }
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getUsername());
    }

    private String createToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    // Extract the user ID from the authorization header/token
    public Integer extractUserId(String authorizationHeader) throws InvalidAuthorizationHeader, UserIdException {
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String jwt = authorizationHeader.substring(7);
            String id = extractUsername(jwt);
            id = id.substring(1);
            try {
                return Integer.parseInt(id);
            } catch(NumberFormatException e) {
                e.printStackTrace();
                throw new UserIdException();
            }
        } else throw new InvalidAuthorizationHeader();
    }
}

