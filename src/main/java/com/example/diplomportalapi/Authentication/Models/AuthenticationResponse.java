package com.example.diplomportalapi.Authentication.Models;

import lombok.Data;

@Data
public class AuthenticationResponse {
    private final String jwt;

}
