package com.example.diplomportalapi.Authentication.Services;

import com.example.diplomportalapi.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Collections;


@Service
public class AuthUserDetailsService implements UserDetailsService {
    private StudentsRepository studentRepository;


    @Autowired
    public AuthUserDetailsService(StudentsRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new org.springframework.security.core.userdetails.User(username,"dtu",Collections.emptyList());
    }
}

