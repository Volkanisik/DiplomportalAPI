package com.example.diplomportalapi.Authentication.Filter;
import com.example.diplomportalapi.Authentication.Services.AuthUserDetailsService;
import com.example.diplomportalapi.Authentication.Util.JwtUtil;
import com.example.diplomportalapi.Metrics.Metrics;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    static Logger logger = Logger.getLogger(JwtRequestFilter.class);

    @Autowired
    private AuthUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Metrics.requestCounter.inc();
        final String authorizationHeader = request.getHeader("Authorization");
        String username = null;
        String jwt = null;
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            jwt = authorizationHeader.substring(7);
            try{
                username = jwtUtil.extractUsername(jwt);
            }catch (Exception e){
                logger.info(request.getMethod()+" | "+request.getRequestURI() + " | " + e.getMessage() + " | IP: " + ipAddress);
                System.out.println(e.getMessage());
                Metrics.tokenfailed.inc();
            }
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

            if (jwtUtil.validateToken(jwt, userDetails)) {
                Metrics.tokenvalidated.inc();
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
            logger.info(request.getMethod()+" | "+request.getRequestURI() + " | " + "User: " + username + " | " + "IP: " + ipAddress);
        }
        filterChain.doFilter(request, response);
    }

}

